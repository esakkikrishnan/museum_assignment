const badRequestError = require("./errors/badrequest.error");
const getMuseumVisitorData = require("./service/museum.service");

function handleMuseum(req, res) {
    const DATE_PARAM = "date";
    const IGNORE_PARAM = "ignore";
    if (!req.query[DATE_PARAM]) {
        return badRequestError(req, res, "Bad Request date query param required");
    }
    const dateQuery = new Date(Number.parseInt(req.query[DATE_PARAM]));

    if (dateQuery == "Invalid Date") {
        return badRequestError(req, res, "Bad Request date query param is invalid");
    }
    const ignoreQuery = req.query[IGNORE_PARAM];
    const result = getMuseumVisitorData(dateQuery, ignoreQuery);
    if (!result) {
        return res.status(400).send("Data not found");
    }
    return res.send(result);
}

module.exports = handleMuseum;