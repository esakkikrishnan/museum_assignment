const PREFIX = "/api"
const ROUTES = {
    VISITOR: `${PREFIX}/visitors`
}

module.exports = {
    PREFIX,
    ROUTES
}