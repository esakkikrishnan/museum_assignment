function badRequestError(req, res, msg, err) {
    return res.status(400).send({
        message: msg || "Bad request",
        status: 400,
        error: err,
    })
}

module.exports = badRequestError;