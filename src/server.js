const express = require("express");
const { ROUTES } = require("./constants/routes");
const handleMuseum  = require("./museum.handler");
const app = express();
const port = 3800;
process.env.TZ = "GMT0BST";

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get(ROUTES.VISITOR, handleMuseum);

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
