const data = require("../../data/museum.data.json");

function getMuseumVisitorData(date, ignore) {
    
    const reqdata = data.find(d => new Date(d.month).getFullYear() === date.getFullYear() && date.getMonth() === new Date(d.month).getMonth());

    if (!reqdata) {
        return undefined;
    }
    const ignoreArr = ["month"];
    if(ignore) {
        ignoreArr.push(ignore);
    }
    const keys = Object.keys(reqdata).filter(k => !ignoreArr.includes(k));
    let highest = {
        museum: "",
        visitors: 0,
    }
    let lowest = {
        museum: "",
        visitors: Number.MAX_VALUE,
    }
    let total = 0;
    for (let i = 0; i < keys.length; i++) {
        const key = keys[i];
        const val = Number.parseInt(reqdata[key]);
        if(val > highest.visitors) {
            highest = {
                museum: key,
                visitors: val
            }
        }
        if (val < lowest.visitors) {
            lowest = {
                museum: key,
                visitors: val
            }
        }
        total += val;
    }
    console.log("date", date.getYear());
    return {
        month: date.toDateString().split(" ")[1],
        year: date.getFullYear(),
        highest,
        lowest,
        total
    }
}

module.exports = getMuseumVisitorData;